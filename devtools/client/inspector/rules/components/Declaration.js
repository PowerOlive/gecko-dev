/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

"use strict";

const { PureComponent } = require("devtools/client/shared/vendor/react");
const dom = require("devtools/client/shared/vendor/react-dom-factories");
const PropTypes = require("devtools/client/shared/vendor/react-prop-types");

const Types = require("../types");

class Declaration extends PureComponent {
  static get propTypes() {
    return {
      declaration: PropTypes.shape(Types.declaration).isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      // Whether or not the computed property list is expanded.
      isComputedListExpanded: false,
    };

    this.onComputedExpanderClick = this.onComputedExpanderClick.bind(this);
  }

  onComputedExpanderClick(event) {
    event.stopPropagation();

    this.setState(prevState => {
      return { isComputedListExpanded: !prevState.isComputedListExpanded };
    });
  }

  renderComputedPropertyList() {
    const { computedProperties } = this.props.declaration;

    if (!computedProperties.length) {
      return null;
    }

    return (
      dom.ul(
        {
          className: "ruleview-computedlist",
          style: {
            display: this.state.isComputedListExpanded ? "block" : "",
          },
        },
        computedProperties.map(({ name, value, isOverridden }) => {
          return (
            dom.li(
              {
                key: `${name}${value}`,
                className: "ruleview-computed" +
                           (isOverridden ? " ruleview-overridden" : ""),
              },
              dom.span({ className: "ruleview-namecontainer" },
                dom.span({ className: "ruleview-propertyname theme-fg-color5" }, name),
                ": "
              ),
              dom.span({ className: "ruleview-propertyvaluecontainer" },
                dom.span({ className: "ruleview-propertyvalue theme-fg-color1" }, value),
                ";"
              )
            )
          );
        })
      )
    );
  }

  renderShorthandOverriddenList() {
    const { declaration } = this.props;

    if (this.state.isComputedListExpanded || declaration.isOverridden) {
      return null;
    }

    return (
      dom.ul({ className: "ruleview-overridden-items" },
        declaration.computedProperties.map(({ name, value, isOverridden }) => {
          if (!isOverridden) {
            return null;
          }

          return (
            dom.li(
              {
                key: `${name}${value}`,
                className: "ruleview-overridden-item ruleview-overridden",
              },
              dom.span({ className: "ruleview-namecontainer" },
                dom.span({ className: "ruleview-propertyname theme-fg-color5" }, name),
                ": "
              ),
              dom.span({ className: "ruleview-propertyvaluecontainer" },
                dom.span({ className: "ruleview-propertyvalue theme-fg-color1" }, value),
                ";"
              )
            )
          );
        })
      )
    );
  }

  render() {
    const { declaration } = this.props;
    const {
      computedProperties,
      isEnabled,
      isKnownProperty,
      isOverridden,
      name,
      value,
    } = declaration;

    return (
      dom.li(
        {
          className: "ruleview-property" +
                     (!isEnabled || !isKnownProperty || isOverridden ?
                      " ruleview-overridden" : ""),
        },
        dom.div({ className: "ruleview-propertycontainer" },
          dom.div({
            className: "ruleview-enableproperty theme-checkbox" +
                        (isEnabled ? " checked" : ""),
            tabIndex: -1,
          }),
          dom.span({ className: "ruleview-namecontainer" },
            dom.span({ className: "ruleview-propertyname theme-fg-color5" }, name),
            ": "
          ),
          dom.span({
            className: "ruleview-expander theme-twisty" +
                       (this.state.isComputedListExpanded ? " open" : ""),
            onClick: this.onComputedExpanderClick,
            style: { display: computedProperties.length ? "inline-block" : "none" },
          }),
          dom.span({ className: "ruleview-propertyvaluecontainer" },
            dom.span({ className: "ruleview-propertyvalue theme-fg-color1" }, value),
            ";"
          )
        ),
        this.renderComputedPropertyList(),
        this.renderShorthandOverriddenList()
      )
    );
  }
}

module.exports = Declaration;
